<?php

namespace Drupal\beacons\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

interface BeaconInterface extends ContentEntityInterface {

}